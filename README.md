# Laser Range Finder (Hokuyo)

![](./figure/hokuyo.png)

## Technical specifications of the Hokuyo UBG-04LX-F01 LiDAR

		Range: 5.6 m
		Scan window: 240°
		Resolution: 0.36°
		Scanning frequency: 35 scans/second
		Scanning time: 28 ms
		Voltage rating: 12 Vdc
		Power consumption: 370 mA max.
		Interfaces: RS232, USB
		Light source: Class 1 laser (785 nm wavelength)
		LMS Laser measurement system
		LRF Laser range finder
		Operating temperature range: -10 to 50°C
		Max. ambient humidity: 85%
		Dimensions: 60 x 75 x 60 mm
		Weight: 260 g


`sudo chmod a+rw /dev/ttyACM0`

`rosrun urg_node urg_node`

# Laser Range Finder (RPLIDAR A3)

![](./figure/rplidar.png)

## RPLIDAR node(with rviz)
`roslaunch rplidar_ros view_rplidar_a3.launch`

## RPLIDAR node
`roslaunch rplidar_ros rplidar_a3.launch`

# Kobuki
`roslaunch kobuki_node minimal.launch` 

# Tip
**ROS_MASTER_URI** 

`export ROS_MASTER_URI=http://localhost:11311` (O)

`export ROS_MASTER_URI=https://localhost:11311` (X)

# Detection
  - Subscriber
    - scan
  - Publisher
    - mobile_base/commands/velocity
      - Linear x,y,z
      - Angular x,y,z



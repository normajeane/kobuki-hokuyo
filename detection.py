#!/usr/bin/env python
import rospy
import math
import numpy as np
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist


def callback(msg):
    global loop_cnt, align_cnt, angle_roi
    angle_min = msg.angle_min
    angle_max = msg.angle_max
    angle_increment = msg.angle_increment
    dist = msg.ranges
    angle_array_size = int((angle_max - angle_min) / angle_increment)
    angle_mid_idx = int(angle_array_size/2)
    angle_roi_idx = int(angle_roi/angle_increment)
    theta = 90 * D2R + angle_roi            # X-right, Y-front (Axis)
    loop_cnt += 1

    print(loop_cnt, ': distance at the forward:', dist[angle_mid_idx], '[m]')
    x = []
    y = []
    for ang_idx, dist_ in enumerate(dist[angle_mid_idx-angle_roi_idx:angle_mid_idx+angle_roi_idx]):
        theta = theta - angle_increment     # scan the data from left to right
        if not math.isnan(dist_):
            x.append(dist_ * math.cos(theta))
            y.append(dist_ * math.sin(theta))
    
    np_x = np.array(x)
    np_y = np.array(y)
    line = np.polyfit(np_x, np_y, 1)
    global hokuyo_offset, thre
    line[0] = line[0] + hokuyo_offset

    if line[1] > 0.5:
        move('f')
    elif line[1] < 0.5:
        stop()
        if line[0] > -thre:
            move('r')
            print('right', line[0])
            align_cnt += 1
        elif line[0] < thre:
            move('l')
            print('left', line[0])
            align_cnt += 1

    if align_cnt > align_thre:              # to avoid oscillation
        stop()


def move(steer):
    vel_cmd_pub = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=10)
    vel_cmd = Twist()
    if steer == 'f':          # Forward
        vel_cmd.linear.x = 0.1
        vel_cmd_pub.publish(vel_cmd)
    elif steer == 'b':        # Back
        vel_cmd.linear.x = -0.1
        vel_cmd_pub.publish(vel_cmd)
    elif steer == 'r':        # Right
        vel_cmd.angular.z = -0.2
        vel_cmd_pub.publish(vel_cmd)
    elif steer == 'l':        # Left
        vel_cmd.angular.z = 0.2
        vel_cmd_pub.publish(vel_cmd)


def stop():
    vel_cmd_pub = rospy.Publisher("mobile_base/commands/velocity", Twist, queue_size=10)
    vel_cmd = Twist()
    vel_cmd.linear.x = 0.0
    vel_cmd.angular.z = 0.0
    vel_cmd_pub.publish(vel_cmd)


if __name__ == '__main__':
    D2R = math.pi / 180
    R2D = 180 / math.pi
    loop_cnt = 0
    align_cnt = 0
    align_thre = 200
    hokuyo_offset = 0.12
    thre = 0.05
    angle_roi = 20 * D2R # total angle = angle_roi * 2

    print('start')
    rospy.init_node('scan_values', anonymous=True)
    rospy.Subscriber('/scan', LaserScan, callback)
    rospy.spin()

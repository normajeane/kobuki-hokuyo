#!/usr/bin/env python

import roslib; roslib.load_manifest('kobuki_node')
import rospy
import curses
import os
import time
import sys

from std_msgs.msg import Empty
from geometry_msgs.msg import Twist
from kobuki_msgs.msg import MotorPower

class BMR_Class:

	def __init__(self, scr):
		#rospy.init_node("getOdom2D", anonymous=True)
		self.scr = scr
		self.vel_cmd_pub = rospy.Publisher("mobile_base/commands/velocity",Twist,queue_size=10)
		self.odom_reset_pub = rospy.Publisher("mobile_base/commands/reset_odometry", Empty, queue_size=10)
		self.power_cmd_pub = rospy.Publisher("mobile_base/commands/motor_power", MotorPower, queue_size=10)
		self.vel_cmd = Twist()
		self.mortor_state = ""
		self.forward = False
		self.back = False
		self.left = False
		self.right = False

	def move(self):
		key = self.scr.getch()
		if key in {90, 122}: #z,Z
			self.power_cmd_pub.publish(MotorPower(MotorPower.ON))
			self.mortor_state = "MortorState : Power ON "
		elif key in {88, 120}: #x,X
			self.power_cmd_pub.publish(MotorPower(MotorPower.OFF))
			self.mortor_state = "MortorState : Power OFF"

		if key == 119:
			self.forward = True
		elif key == 115:
			self.back = True
		elif key == 97:
			self.left = True
		elif key == 100:
			self.right = True
		elif key == 114:
			self.odom_reset_pub.publish()

		if self.forward and self.back==False:
			self.vel_cmd.linear.x = 0.1
		elif self.forward==False and self.back:
			self.vel_cmd.linear.x = -0.1
		elif self.forward and self.back:
			self.vel_cmd.linear.x = 0
			self.forward, self.back = False, False

		if self.right and self.left==False:
			self.vel_cmd.angular.z = -0.5
		elif self.right==False and self.left:
			self.vel_cmd.angular.z = 0.5
		elif self.right and self.left:
			self.vel_cmd.angular.z = 0
			self.right, self.left = False, False
		

		self.vel_cmd_pub.publish(self.vel_cmd)

	def display(self):

		self.scr.move(1,0)
		self.scr.addstr(1,0,"=========================")
		self.scr.move(2,0)
		self.scr.addstr(2,0,"= 'r' : Odometry Reset  =")
		self.scr.move(3,0)
		self.scr.addstr(3,0,"= 'z' : Motor Power ON  =")
		self.scr.move(4,0)
		self.scr.addstr(4,0,"= 'x' : Motor Power OFF =")
		self.scr.move(5,0)
		self.scr.addstr(5,0,"=========================")

		self.scr.move(7,0)
		self.scr.addstr(7,0,"[Motor State]")
		self.scr.move(8,0)
		self.scr.addstr(8,0,self.mortor_state)
		self.scr.move(10,0)
		self.scr.addstr(10,0,"Press w,s,a,d...")

def main(args):
	rospy.init_node("key_op", anonymous=True)
	stdscr = curses.initscr()
	stdscr.nodelay(1)
	curses.noecho()
	bmr_class = BMR_Class(stdscr)
	r = rospy.Rate(10)
	try:
		while not rospy.is_shutdown():
			bmr_class.move()
			bmr_class.display()
			stdscr.refresh()
			r.sleep()
	except KeyboardInterrupt:
		print("Shutting Down")
	#Clean up curses
	curses.nocbreak()
	stdscr.keypad(0)
	curses.echo()
	curses.endwin()

if __name__ == '__main__':
	os.system("clear")
	main(sys.argv)
